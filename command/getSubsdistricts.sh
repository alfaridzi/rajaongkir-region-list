#!/bin/bash
set -B                  # enable brace expansion
for i in {1..501}; do
	echo 'scrapping city_id '$i
	curl --request GET --url 'https://pro.rajaongkir.com/api/subdistrict?city='$i --header 'key: 0c0ca70063f90a804ec3422b1984eb24' > $i'.json'
done
